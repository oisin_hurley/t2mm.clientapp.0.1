#!/usr/bin/python
from __future__ import division
import psycopg2
from datetime import datetime
import sys


hostname = 'localhost'
username = 'brianfinnerty'
password = ''
database = 't2mm_dev1'


# Simple routine to run a query on a database and print the results:
def calculate_daily_stats(conn):
    cur = conn.cursor()
    word_cnt = 0
    duration_cnt = 0
    ttw_total = ''
    thew_cnt = ''
    users = []

    #print 'Argument List:', str(sys.argv)
    # start_date = datetime.strftime(datetime.date(sys.argv[1] + ' 00:00:00'), '%d-%m-%Y %H:%M:%S')
    # dt_str = sys.argv[1] + ' 00:00:00'
    run_date = str(sys.argv[1])
    # start_date = datetime.strptime(sys.argv[1] + ' 00:00:00', "%Y-%m-%d %H:%M:%S")
    # end_date = datetime.strptime(sys.argv[1] + ' 23:59:59', "%Y-%m-%d %H:%M:%S")
    # end_date = datetime(sys.argv[1] + " 23:59:59")

    cur.execute('SELECT id, email FROM users')
    for id, username in cur.fetchall():
        users.append(id)

    #print users
    users = [3]

    for user_id in users:

        #print user_id
        # cur.execute('SELECT user_id, top_ten_words, word_count, start_time, end_time, duration FROM recording '
        #             'where user_id = {0} '
        #             'AND start_time >= {1} '
        #             'AND start_time < {2} '.format(user_id, "\"2015-12-29 00:00:00\"", "\"2015-12-29 23:00:00\""))
        cur.execute('SELECT user_id, top_ten_words, word_count, start_time, end_time, duration FROM recording where user_id = {0} '.format(user_id))
        cnt = 0
        word_cnt = 0
        duration_cnt = 0
        ttw_total = ''
        word_list = []
        for user_id, top_ten_words, word_count, start_time, end_time, duration in cur.fetchall():
            # print start_time
            if run_date[:10] == str(start_time)[:10]:
                #print 'record valid'
                # print user_id, top_ten_words
                word_cnt = word_cnt + word_count
                if duration is None:
                    duration = 0

                duration_cnt = duration_cnt + duration
                if cnt == 0:
                    ttw_total = ttw_total + top_ten_words
                else:
                    ttw_total = ttw_total + ',' + top_ten_words
                cnt += 1

        # split and find top 10 for words for the week
        if len(ttw_total) > 0:
            word_list = ttw_total.split(',')
        dict_total = {}
        cnt = 0
        # place words in dictionary
        words = []

        for word in word_list:
            #print word
            key_value = word.split(': ')
            if len(key_value) == 2:
                if key_value[0] in words:
                    dup_word_cnt = int(key_value[1]) + dict_total[key_value[0]]
                    del dict_total[key_value[0]]
                    dict_total[key_value[0]] = int(dup_word_cnt)
                else:
                    print key_value
                    dict_total[key_value[0]] = int(key_value[1])
                    words.append(key_value[0])
        top_ten = ""
        tt_cnt = 0
        for w in sorted(dict_total, key=dict_total.get, reverse=True):
            # while tt_cnt < 10:
            #print w, dict_total[w]
            if tt_cnt == 0:
                top_ten = w + ": " + str(dict_total[w])
            else:
                top_ten = top_ten + "," + w + ": " + str(dict_total[w])
            tt_cnt += 1
            if tt_cnt == 10:
                break
        avg_words_per_hr = 0.0
        avg = 0.0
        if word_cnt != 0:
            # if duration_cnt < 3600:
            avg = float(word_cnt / duration_cnt)
            avg_words_per_hr = int(avg * 3600)
            #print 'avg word count {0}'.format(avg_words_per_hr)
            # else:

        #print datetime.now()
        dt = datetime.now()
        cur.execute('INSERT INTO stats_daily (user_id,word_count, top_ten_words, total_rec_time, "createdAt",avg_words_per_hour, date) VALUES (%s, %s, %s, %s, %s, %s, %s)', (user_id, word_cnt, top_ten, duration_cnt, dt,avg_words_per_hr,run_date))

        conn.commit()
        #print dict_total


def main():
    #print 'Using psycopg2'

    myConnection = psycopg2.connect( host=hostname, user=username, password=password, dbname=database )
    calculate_daily_stats(myConnection)
    myConnection.close()

    # print 'Using PyGreSQL'
    #
    # myConnection = pgdb.connect( host=hostname, user=username, password=password, database=database )
    # doQuery(myConnection)


if __name__ == '__main__':
    main()