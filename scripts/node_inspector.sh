#!/usr/bin/env bash

source env/bin/activate;
node-inspector --preload=false --cli --save-live-edit=true --debug-brk --web-port=9000;