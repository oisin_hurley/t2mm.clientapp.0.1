--
-- Any Alterations to the Database should be included here with a date and user
-- Then commit to git and run manually eg. psql {database} < scripts/alter_database.sql
--

-- 27/01/2016, Brian Finnerty, TEST Alteration
-- ALTER TABLE users OWNER TO brianfinnerty; -- my local machine
-- ALTER TABLE users OWNER TO brian; -- remote server
-- ALTER TABLE users OWNER TO postgres; -- back to normal on both