var passport = require('passport'),
    userController = require('../controllers/userController.js'),
    isAuthenticated = require('../utils/isAuthenticated.js');   

module.exports = function(express) {
    var router = express.Router();

    // Log in / out routes
    router.get('/users/login/', userController.showLogin);
    router.post('/users/login/', passport.authenticate('local', {
        failureRedirect: '/users/login/',
        failureFlash: true 
    }), function(req, res, next) {
            req.session.save(function (err) {
                if(err){
                    return res.status(500).end();
                }else{
                    res.redirect('/');
                }
            });
        });


    router.get('/users/logout', userController.logout);


    // Sign up routes
    router.get('/users/signup/', userController.show);
    router.post('/users/signup/', userController.signup);

    // Account
    router.get('/users/account/', isAuthenticated, userController.showAccount);
    router.post('/users/account/', isAuthenticated, userController.updateAccount);

    return router
}

