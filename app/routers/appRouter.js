var passport = require('passport'),
    appController = require('../controllers/appController.js'),
    isAuthenticated = require('../utils/isAuthenticated.js');   

module.exports = function(express) {

    var router = express.Router();

    router.get('/', isAuthenticated, appController.redirectToDash);

    //router.get('/dashboard(/week-:week/)?', isAuthenticated, appController.showDashboard);
    router.get('/dashboard(/week-:week/)?', isAuthenticated, appController.showDashboard);

    router.get('/feedback/', isAuthenticated, appController.showFeedback);

    router.post('/feedback/', isAuthenticated, appController.addFeedback);

    return router
};

 