
module.exports.dateRangeWordCount = function(userId, startDate, endDate){

	startDate = startDate.format('YYYY-MM-DD');
	endDate = endDate.format('YYYY-MM-DD');

	var query = 'SELECT SUM(word_count) AS word_count, DATE_TRUNC(\'day\', end_time) AS day ';
	query += ' FROM recording AS r ';
	query += ' WHERE r.user_id = ' + userId ;
	query += ' AND end_time >= \'' + startDate + '\'';
	query += ' AND end_time < \'' + endDate + '\'';
	query += ' GROUP BY day ';
	query += ' ORDER BY day';

	return query;

};


/*

WHERE
      login_date >= '2014-02-01'
  AND login_date <  '2014-03-01'

  */
