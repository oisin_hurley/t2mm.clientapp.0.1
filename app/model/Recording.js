var Sequelize = require('sequelize'),
	User = require('./User.js');

var attributes = {
 
	user_id: {
		type: Sequelize.INTEGER,
		references: {
			model: User,
			key: 'id',
			deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE
		}
	},

	start_time : {
		type: Sequelize.DATE
	},

	end_time : {
		type: Sequelize.DATE
	},

	word_count: {
		type: Sequelize.INTEGER
	},

	file_part: {
		type: Sequelize.INTEGER
	},

	total_split: {
		type: Sequelize.INTEGER
	},

	transcribe_time:{
		type: Sequelize.INTEGER
	},

	top_ten_words: {
		type: Sequelize.STRING(1000)
	},

	top_ten_themed_words: {
		type: Sequelize.STRING(1000)
	},

	folder_name:{
		type: Sequelize.STRING(100)
	},

	file_name:{
		type: Sequelize.STRING(1000)
	}

}

var options = {
  freezeTableName: true
}

module.exports.attributes = attributes
module.exports.options = options