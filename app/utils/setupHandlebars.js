var ehandlebars = require('express-handlebars'),
  moment = require('moment');

module.exports = function(app) {
  var hbs = ehandlebars.create({
    defaultLayout: 'app',
    //partialsDir: ['partials'],
    helpers: {
      section: function(name, options) {
        if (!this._sections) this._sections = {}
        this._sections[name] = options.fn(this)
        return null
      },
      formatDate(date, format, options){
        if(format.constructor === Object){
          options = format;
          format = 'YYYY-MM-DD';
        }
        
        return moment(date).format(format);
      }
    }
  })

  app.engine('handlebars', hbs.engine)
  app.set('view engine', 'handlebars')
}