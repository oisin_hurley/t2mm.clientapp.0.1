var bcrypt = require('bcrypt'),
    Model = require('../model/models.js'),
    _ = require('lodash'),
    moment = require('moment');

module.exports.show = function(req, res) {
  res.render('signup', {layout: 'body'})
};

module.exports.signup = function(req, res) {

  var email = req.body.email;
  var password = req.body.password;
  var password2 = req.body.password2;
  
  if (!email || !password || !password2) {
    req.flash('error', "Please, fill in all the fields.");
    res.redirect('/users/signup/')
  }
  
  if (password !== password2) {
    req.flash('error', "Please, enter the same password twice.");
    res.redirect('/users/signup/')
  }
  
  var salt = bcrypt.genSaltSync(10);
  var hashedPassword = bcrypt.hashSync(password, salt);
  
  var newUser = {
    email: email,
    salt: salt,
    password: hashedPassword
  };
  
  Model.User.create(newUser).then(function() {
    res.redirect('/')
  }).catch(function(error) {
    req.flash('error', "Please, choose a different email.");
    res.redirect('/users/signup/')
  })
};

module.exports.showLogin = function(req, res) {
    // var session = require('express-session');
    // var SequelizeStore = require('connect-session-sequelize')(session.Store);
    // debugger;
    // var sequelizeStore = new SequelizeStore({db: Model.Recording.sequelize});
    res.render('login', {layout: 'body'});
};


module.exports.logout = function(req, res) {
    req.logout();
    res.redirect('/');
};

module.exports.showAccount = function(req, res) {
    var user = req.user;
    user.child_dob_string = moment(user.child_dob).format('YYYY-MM-DD');
    res.render('account', {user:user});
};

module.exports.updateAccount = function(req, res, next) {

    var user = req.user;

    var updatedData = _.merge(req.body, {
        mothers_contact_no : convertToNum(req.body.mothers_contact_no),
        fathers_contact_no : convertToNum(req.body.fathers_contact_no),
        child_dob : convertToDate(req.body.child_dob),
        activation_date: convertToDate(req.body.activation_date)
    });

    var validData = _.pick(updatedData, function(v, k) {
        return !!v;
    });

    user.update(validData)
      .then(function(user){
          return res.render('account', {user:user});
      })
      .catch(function(err){
          next(err);
      });
 
};

var convertToNum = function(string){
    var newNum = parseInt(string, 10);
    if(_.isNaN(newNum)){
      return null;
    }else{
      return newNum;
    }
};

var convertToDate = function(dateString){
    var newDate = Date.parse(dateString);
    if(_.isNaN(newDate)){
      return null;
    }else{
      return new Date(newDate);
    }
};

