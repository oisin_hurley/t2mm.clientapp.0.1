var Model = require('../model/models.js'),
	RecordingQueries = require('../sql/recording.js'),
	moment = require('moment');


module.exports.redirectToDash = function(req, res, next) {
    res.redirect('/dashboard/');
};


var getDashboardDataFromRecording = function(userId, startDate, endDate){

	var query = RecordingQueries.dateRangeWordCount(userId, startDate, endDate);
	var sequelize = Model.connection;

	return sequelize.query(query, { 
		type: sequelize.QueryTypes.SELECT
	})
	.then(function(counts) {
    	return counts;
    })
	.catch(function(err){
		throw err;
	});
};

var getDashboardDataFromStatsDaily = function(userId, startDate, endDate){

	return Model.StatsDaily.findAll({
        //attributes: ['word_count', ['date', 'day']],
        where : {
            user_id : userId,
            date: {
                $lte: endDate,
                $gt: startDate
            }
        },
        order: ['date']
    }).then(function(data){
        return data;
    })
    .catch(function(err){
        throw err;
    });
};



module.exports.showDashboard = function(req, res, next) {

	var endDate, startDate; 

	if(req.params.week){
		var weekNo = parseInt(req.params.week, 10);
		endDate = moment(req.user.activation_date).add(weekNo, 'week');
		startDate = moment(req.user.activation_date).add(weekNo, 'week').subtract(7, 'days');
	}else{
		startDate = moment(req.user.current_week_start);
		endDate = moment();
	}

	var weeks = Array.apply(null, {length: req.user.current_week - 1}).map(function(nada, i){return i+1});

	getDashboardDataFromStatsDaily(req.user.id, startDate.toDate(), endDate.toDate())
	.then(function(data) {

        var counts = data.map(function(obj){
            return {
                'word_count': obj.word_count,
                'day' : obj.date
            }
        });

    	res.render('dashboard', {
			    user : req.user,
			    counts: JSON.stringify(counts),
			    startDate : startDate.format('DD/MM/YYYY'),
			    endDate : endDate.format('DD/MM/YYYY'),
			    weeks : weeks
			});	
		})
	.catch(function(err){
		next(err);
	});
};

module.exports.updateDashboard = function(req, res, next) {

	var startDate = new Date(),
		endDate = new Date();

	getDashboardData(startDate, endDate)
	.then(function(counts) {
    	res.json(counts);	
	})
	.catch(function(err){
		next(err);
	});
};

module.exports.showFeedback = function(req, res, next) {

	Model.Feedback.findAll()
		.then(function(comments){
			res.render('feedback', {
			    user : req.user,
			    previous_comments: comments
			});	
		})
		.catch(function(err){
			next(err);
		});
};


module.exports.addFeedback = function(req, res, next) {

	var newComment = {
	    user_id: req.user.id,
	    comments: req.body.comments
	};

	Model.Feedback.create(newComment)
		.then(function(comment){
			req.flash('success', "Comment Received! Thank you very much for your feedback.");
			res.redirect('/feedback/');
		})
		.catch(function(err){
			next(err);
		});
};